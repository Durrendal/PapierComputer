#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
LUA_ROCKS ?= /usr/bin/luarocks-5.3
DESTDIR=/usr/bin

fetch-libraries:
	$(LUA_ROCKS) install inspect --local
	$(LUA_ROCKS) install luacheck --local

compile-lua:
	fennel --compile --require-as-include src/fennel/papier.fnl > src/fennel/papier.lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/fennel/papier.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/fennel/papier.lua

install-lua:
	install ./src/fennel/papier.lua -D $(DESTDIR)/papier

#Ask Techno about this one
compile-bin:
	cd ./src/fennel && fennel --compile-binary papier.fnl papier-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/fennel/papier-bin -D $(DESTDIR)/papier

check:
	luacheck ./src/fennel/papier.lua
