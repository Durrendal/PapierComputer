#!/usr/bin/fennel
(local inspect (require "inspect"))

;;=====[ Globals ]=====
;;MEM represents our register values, there are 8, each a table of values
;;3 in register 1 and 4 in register 2 = [ {1,1,1}, {1,1,1,1} ]
(var mem [ [] [] [] [] [] [] [] [] ])
(global instructions [])

;;=====[ Utility ]=====
(fn usage []
  (print "Usage: papier program.asm
Provided an assembly file with WDR Papier ASM, parse and execute.

OpCodes:
ADD [reg]       |  Add 1 to given register.
DEC [reg]       |  Decrease given register by 1.
ISZ [reg]       |  Check is register is 0, if not continue. If zero, skip 1 line and continue.
JMP [line]      |  Jump to specified line.
STP             |  Stop program execution.
REG [reg] [val] | Set the given register to the provided value.

Example:
Provided register = 5 and register 2 = 3 add register into register 1

add_into.asm:
REG 1 5
REG 2 3
ISZ 2
JMP 4
STP
ADD 1
DEC 2
JMP 1
")
         (os.exit))

;;Split a string, return a table {"ADD", 3} {"ISZ", 5}
(fn util/split [val check]
  "Split string on delim and return as table"
  (if (= check nil)
      (do
        (local check "%s")))
  (local t {})
  (each [str (string.gmatch val (.. "([^" check "]+)"))]
    (do
      (table.insert t str)))
  t)

;;Convert a value into a table of 1's
;;util/accumulate 5 -> [1 1 1 1 1]
(fn util/accumulate [val]
  (let
      [reg []]
    (for [i 1 val]
      (table.insert reg 1))
    reg))

;;=====[ OpCodes ]=====

;;Add one to a specified register
(fn ADD [register]
  (let
      [reg (. mem register)]
    (table.insert reg 1)
    (tset mem register reg)))

;;Remove one from a specified register
(fn DEC [register]
  (let
      [reg (. mem register)]
    (table.remove reg 1)
    (tset mem register reg)))

;;Check if a specified register is 0/nil. If = 0/nil skip one line and continue. If != 0, continue
(fn ISZ [register]
  (if (or (= (. mem register 1) nil) (= (. mem register 1) 0))
      true ;;return true to execinstr to initiate skip
      ;;= 0 skip one continue
      false ;;return false to execinstr to continue to next op
      ;;~= 0 continue
      ))

;;JMP function is implemented as a jump to curop
;;JMP 4 -> (set curop 4)

;;Stop program execution
(fn STP []
  (print "Program execution halting..")
  (var memory "")
  (each [k v (pairs mem)]
    (set memory (.. memory "{" (length v) "} ")))
  (print (.. "{ " memory "}"))
  (print (inspect mem))
  (os.exit))

;;=====[ Parsing ]=====
;;Serialize instruction set from basic papier asm into a table
;; ISZ 1 -> [ { "ISZ", 1 } ]
(fn serinstr [code]
  (each [line (io.lines code)]
    (if (or (~= line "") (~= line nil) (~= line "\n"))
        (let
            [data (util/split line " ")
             opcode (. data 1)
             reg/line (. data 2)]
          (if (or (= opcode "REG") (= opcode "reg"))
              (tset mem (tonumber reg/line) (util/accumulate (tonumber (. data 3))))
              (table.insert instructions [ opcode reg/line ]))))))

;;Parse instructions and execute program flow
(fn execinstr [instr]
  (var curop 1)
  (while true
    (let
        [opcode (. instructions curop 1)
         reg/line (tonumber (. instructions curop 2))]
      (print opcode reg/line curop)
      (if (or (= opcode "ADD") (= opcode "add"))
          (do
            (ADD reg/line)
            (set curop (+ 1 curop)))
          (or (= opcode "DEC") (= opcode "dec"))
          (do
            (DEC reg/line)
            (set curop (+ 1 curop)))
          (or (= opcode "ISZ") (= opcode "isz"))
          (do
            (if (ISZ reg/line)
                (set curop (+ curop 2))
                (set curop (+ 1 curop))))
          (or (= opcode "JMP") (= opcode "jmp"))
          (set curop reg/line)
          (or (= opcode "STP") (= opcode "stp"))
          (STP)
          (do
            (io.stderr:write (.. opcode " is not a valid opcode! Aborting..\n"))
            (os.exit))))))

;;=====[ Main ]=====
(fn main [?argv1]
  (if (or (= ?argv1 nil) (= ?argv1 "-h") (= ?argv1 "--help"))
      (usage)
      (do
        (serinstr ?argv1)
        (execinstr instructions))))

(main ...)
