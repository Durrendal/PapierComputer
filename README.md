# What?
A WDR Paper Computer emulator.

If you're curious about what the WDR Paper Computer is, you can read a little bit more about it here. 
https://en.wikipedia.org/wiki/WDR_paper_computer

But at its core it's just a simple teaching mechanism for basic assembly. It's Turing complete too, which is pretty cool. 

We're stuck inside due to a blizzard today, so I decied to write one in Fennel. I might go back and rewrite this in a few different languages. It's a fun little project and I think there's some merit in using it as a project to learn other languages.

# Compiling
Alpine Deps:
```
make fennel5.3 lua5.3 lua5.3-dev luarocks
```

Luarocks:
```
inspect (I may switch this to fennelview to remove the dep)
```

Compile a static binary:
```
make compile-bin
```

Compile a lua version:
```
make compile-lua
```

# Usage:
```
Usage: papier program.asm
Provided an assembly file with WDR Papier ASM, parse and execute.
```

There are some example ASM files in the asm directory of this repo, if you'd like to just test papier against them they should work as is. If you'd like to write your own the OpCodes to do so are as follows.

# OpCodes:
ADD [reg]
adds 1 to given register

```
ADD 1
```

DEC [reg]
Subtracts 1 from given register

```
DEC 2
```

JMP [line]
Jump to provided line and continue execution

```
JMP 14
```

ISZ [reg]
Check if the register is zero. If it isn't zero, continue onto next line. If it is zero, skip the next line and continue

```
ISZ 4
```

STP
Stop the program execution

```
STP
```

REG [reg] [val]
Set the registry specified to a specific value

```
REG 1 5
```

# Papier Output:

Papier reports each step of program execution in three values
OpCode | Register/Line | Current Execution Point

For example;
ISZ 2   1
---------
ISZ is the opcode
2 is the register ISZ is checking
1 is the current execution line in the ASM code.

JMP 1   6
---------
JMP is the opcode
1 is the line JMP will jump to
6 is the current execution line in the ASM code

When papier finishes execution it returns the values in the registers at the end of execution. In the example below the registers look like this
```
REG 1 = 5
REG 2 = 3
```
and the register after execution is
```
{ { 1, 1, 1, 1, 1, 1, 1, 1 }, {}, {}, {}, {}, {}, {}, {} }
```
This can be read as REG 1 = 8. Each register is a series of 1's contained in a lua table, I might change that later to just report the registers as totals.

Anyways, here's the out of add_into.asm in its entirety.


```
ISZ	2	1
JMP	4	2
ADD	1	4
DEC	2	5
JMP	1	6
ISZ	2	1
JMP	4	2
ADD	1	4
DEC	2	5
JMP	1	6
ISZ	2	1
JMP	4	2
ADD	1	4
DEC	2	5
JMP	1	6
ISZ	2	1
STP	nil	3
Program execution halting..
{ {8} {0} {0} {0} {0} {0} {0} {0} }
{ { 1, 1, 1, 1, 1, 1, 1, 1 }, {}, {}, {}, {}, {}, {}, {} }
```


# Attribution:
Paper Icon:
Paper by Nursila from NounProject.com

The WDR Paper Computer was developed by Wolfgang Back and Ulrich Rohde