REG 1 3  --If reg1 = odd, reg 2 will have 1 in it at the end.
ISZ 1    --Is 1 0? If so stop, else jump to 4
JMP 4   
STP
DEC 1    --Reduce register by 1
ISZ 2    --Is 2 0? If so jump to 9, else increment register 2 by 1
JMP 9
ADD 2
JMP 1    --Return to line 1
DEC 2    --If register 2 was 0, reduce register 2 by 1
JMP 1    --return to line 1