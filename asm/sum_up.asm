REG 2 4 --Return sum of all numbers up to. 4 = (+ 1 2 3 4) 10
ISZ 2   --Is reg 2 0? If so stop, else onto line 4
JMP 4
STP
ISZ 2   --Is register 2 0? If so onto line 11, else line 7
JMP 7
JMP 11
DEC 2  --If reg 2 was 0, dec reg2 by 1
ADD 1  --Add 1 to reg1
ADD 3  --Add 1 to reg3
JMP 4  --Jump to line 4, repeated reg 2 check until empty, then onto line 11
DEC 3  --Dec reg3 ny 1
ISZ 3  --Is reg 3 0? If so back to line 1 to complete, else down to line 15
JMP 15
JMP 1
DEC 3  --Dec reg3 by 1
ADD 2  --Add 1 to reg 1
JMP 12 --Rechck reg3 and repeat until empty