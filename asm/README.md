# What?
Some simple pseudo-assembly that works with papier, modified slightly (REG calls really). I've added the source for each file next to the title, if there isn't a source then it's one I contributed.

## add_into.asm (Chris Staecker)
Add register 2 into register 1

## clear_register.asm (EduNet Nambia) 
Empty register 1

## copy_into.asm (EduNet Nambia)
Copy register 2 into register 1

## multiply.asm (Chris Staecker)
Multiply the values of register 2 by register 3 and return the solution to register 1

## subtract_from.asm (EduNet Nambia)
Subtract register 2 from register 1

## times_two.asm
Take a value in register 1 and return it x2 in register 2

## even_or_odd.asm (Chris Staecker)
Check if value in register 1 is even or odd. End with 0 in register 2 if even, and 1 in register 2 if odd

## sum_up.asm (Chris Staecker)
Break the number in reg 1 into each number leading up to it and return its sum. IE if register 1 = 4 then (+ 1 2 3 4) = 10